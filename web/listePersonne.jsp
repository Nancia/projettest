<%-- 
    Document   : listePersonne
    Author     : P14_B_64_Nancia
--%>
<%@page import="Model.Personne"%>
<%@page import="java.util.ArrayList"%>
<%
    ArrayList<Personne> liste = (ArrayList<Personne>) request.getAttribute("liste_personne");
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <table width="600" border="1">
            <tr>
                <th>Id</th>
                <th>Nom</th>
                <th>Age</th>
                <th>Date de naissance</th>
                <th>Sexe</th>
            </tr>
            <% for(int i=0; i<liste.size(); i++){ %>
            <tr>
                <td><% out.println(liste.get(i).getId()); %></td>
                <td><% out.println(liste.get(i).getNom()); %></td>
                <td><% out.println(liste.get(i).getAge()); %></td>
                <td><% out.println(liste.get(i).getNaissance()); %></td>
                <td><% out.println(liste.get(i).getSexe()); %></td>
            </tr>
            <% } %>
        </table>
    </body>
</html>
