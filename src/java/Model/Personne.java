/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Connexion.Connexion;
import ModelView.ModelView;
import Utilitaire.Annotation;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 *
 * @author P14_B_64_Nancia
 */
public class Personne{
    int id;
    String nom;
    int age;
    Date naissance;
    String sexe;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Date getNaissance() {
        return naissance;
    }

    public void setNaissance(Date naissance) {
        this.naissance = naissance;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }
    
    @Annotation(url = "allPersonne")
    public ModelView getAllPersonne() throws Exception{
        ModelView mv = new ModelView();
        Connection connect = new Connexion().getConnexion();
	PreparedStatement st = null;
        ResultSet rs = null;
        ArrayList<Personne> liste = new ArrayList<Personne>();
        try{
            String req = "select * from personne";
            st = connect.prepareStatement(req);
            rs = st.executeQuery();
            while(rs.next())
	    { 
                Personne p = new Personne();
                p.setId(rs.getInt("id"));
                p.setNom(rs.getString("nom"));
                p.setAge(rs.getInt("age"));
                p.setNaissance(rs.getDate("naissance"));
                p.setSexe(rs.getString("sexe"));
                liste.add(p);
            }
            
            mv.setUrl("listePersonne.jsp");
            HashMap<String, Object> data = new HashMap<String, Object>();
            data.put("liste_personne", liste);
            mv.setData(data);
        }
        catch(Exception e){
            throw e;
        }
        finally{
            connect.close();
            if(rs != null){
                rs.close();
            }
            if(st != null){
                st.close();
            }
        }
        return mv;
    }
    
    @Annotation(url = "insertPersonne")
    public ModelView insertPersonne() throws Exception{
        ModelView mv = new ModelView();
        mv.setUrl("/allPersonne.do");
        mv.setData(null);
        Connection connect = new Connexion().getConnexion();
	PreparedStatement st = null;
        ResultSet rs = null;
        try{
            String req = "insert into personne(nom,age,naissance,sexe) values (?,?,?,?)";
            st = connect.prepareStatement(req);
            st.setObject(1, this.nom);
            st.setObject(2, this.age);
            st.setObject(3, this.naissance);
            st.setObject(4, this.sexe);
            st.executeUpdate();
        }
        catch(Exception e){
            throw e;
        }
        finally{
            connect.close();
            if(rs != null){
                rs.close();
            }
            if(st != null){
                st.close();
            }
        }
        return mv;
    }
}
